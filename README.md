# GameOfMaths_GrA

Projet GameOfMaths, Groupe A


1. **Présentation rapide :**

    Le projet est Game Of Maths est un site web ludique pour les classes de notre client LE MELLEC Frédéric qui est professeur en mathématiques.
    Le projet est à un maître d'oeuvre qui est ROIRAND Xavier, l'enseignant tuteur des membres du projet.

2. **Les membres du projet**

    - **Chef de projet :** QUACH Valentin
    - **Responsable de communication :** BUAN Kilian
    - **Responsable de documentation :** CHASTANIER Tanguy
    - **Responsable des tests :** LENOBLE Alexandre


3. **Manuels**

    Ce dossier contient le manuel utilisateur ainsi que le manuel d'installation du projet.

4. **Src**

    Ce dossier contient deux sous-dossier chacun abritant un projet npm.
    Le répertoire *game_of_maths* contient l'application client vue.
    Le répertoire *game_of_maths_express* contient l'application express qui fournit l'application web.

5. **Data**

    Contient le logo du projet.

7. **Delivrables**

    Contient les documents concernant les sprints (spécifications et bilan de sprint)

6. **Docs**

    Contient la proposition de projet
