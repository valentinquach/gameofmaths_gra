if [ $0 = 0]
then
	echo 'API_URL='$1'/request' > .env
else
	echo 'Veuillez fournir au moins un argument'
    echo 'Usage :'
    echo 'set_url <url>'
fi