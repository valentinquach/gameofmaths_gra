const sqlite3 = require('sqlite3');

let db = new sqlite3.Database('./database/game_of_maths.db', (err) => {
    if (err) {
        console.log(err);
        return console.error(err.message);
    }
    console.log('db > Connected to database.');
});

module.exports = db;
