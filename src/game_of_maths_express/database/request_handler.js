module.exports.handle = function(request){
    return new Promise(function(resolve,reject){
        if(request.procedure){
            try{
                let procedure = require("./procedure/"+request.procedure+".js")
                let request_keys = Object.keys(request)
                let fields_provided = true
                let missing_field = undefined
                procedure.needed_fields.forEach(field => {
                    if(!request_keys.includes(field)){
                        fields_provided = false
                        missing_field = field
                        reject({err:"missing field \""+missing_field+"\""})
                    }
                });
                if(fields_provided){
                    procedure.process(request)
                    .then((result)=>{resolve(result)})
                    .catch((err)=>{reject(err)})
                }
            }catch(err){
                console.log(err)
                reject({err:"procedure \""+procedure+"\" not found"})
            }
        }else{
            reject({err:"no attribute \"procedure\" in request"})
        }

    })
}