PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE Eleve (
    id_eleve VARCHAR2(40) PRIMARY KEY,
    id_login_eleve VARCHAR2(32) unique NOT NULL,
    mdp_eleve VARCHAR2(50) NOT NULL,
    nom_eleve VARCHAR2(20) NOT NULL,
    prenom_eleve VARCHAR2(20) NOT NULL,
    points_eleve NUMBER(10),
    classe_eleve VARCHAR2(20) NOT NULL,
    CONSTRAINT fk_classe_eleve FOREIGN KEY (classe_eleve) REFERENCES Classe(id_classe)
);

CREATE TABLE Groupe (
    id_groupe VARCHAR2(40) PRIMARY KEY,
    nom_groupe VARCHAR2(32) NOT NULL,
    chef_groupe VARCHAR2(40) NOT NULL,
    CONSTRAINT fk_id_eleve FOREIGN KEY (chef_groupe) REFERENCES Eleve(id_eleve)
);

CREATE TABLE Classe (
    id_classe VARCHAR2(40) PRIMARY KEY,
    nom_classe VARCHAR2(20) NOT NULL
);

CREATE TABLE Chateau (
    id_chateau VARCHAR2(40) PRIMARY KEY,
    nom_chateau VARCHAR2(40),
    groupe_chateau VARCHAR2(40) ,
    CONSTRAINT fk_groupe_chateau FOREIGN KEY (groupe_chateau) REFERENCES Groupe(id_groupe)
);

CREATE TABLE Theme (
    id_theme VARCHAR2(40) PRIMARY KEY,
    nom_theme VARCHAR2(32) NOT NULL
);
CREATE TABLE Attaque(
    id_attaque VARCHAR2(40) PRIMARY KEY,
    chateau_attaque VARCHAR2(40) NOT NULL,
    groupe_attaque VARCHAR2(40) NOT NULL,
    lance_attaque VARCHAR(40) NOT NULL CHECK (lance_attaque IN ("ATTENTE", "LANCER")),
    questionnaire_attaque VARCHAR(40),
    CONSTRAINT fk_chateau_attaque FOREIGN KEY (chateau_attaque) REFERENCES Chateau(id_chateau),
    CONSTRAINT fk_groupe_attaque FOREIGN KEY (groupe_attaque) REFERENCES Groupe(id_groupe),
    CONSTRAINT fk_questionnaire_attaque FOREIGN KEY (questionnaire_attaque) REFERENCES Questionnaire(id_questionnaire)
);

CREATE TABLE Questionnaire (
    id_questionnaire VARCHAR2(40) PRIMARY KEY,
    titre_questionnaire VARCHAR2(32) NOT NULL,
    theme_questionnaire VARCHAR2(40) NOT NULL,
    --difficulte_questionnaire VARCHAR2(10) NOT NULL CHECK (difficulte_questionnaire IN ("FACILE", "MOYEN", "DIFFICILE")), --! erreur: difficulte_reponse et pas type
    temps_questionnaire NUMBER(1) NOT NULL, --Le temps est donné en secondes. Toutes les questions du qcm devront être répondues dans cette limite de temps.
    CONSTRAINT fk_theme_questionnaire FOREIGN KEY (theme_questionnaire) REFERENCES Theme(id_theme)
);

CREATE TABLE Question (
    id_question VARCHAR2(40) PRIMARY KEY,
    titre_question VARCHAR2(32) NOT NULL,
    type_question VARCHAR2(10) NOT NULL CHECK (type_question IN ("QCM", "OUVERTE")),
    points_question NUMBER(4) NOT NULL,
    bonne_reponse_question NUMBER(1) NOT NULL,
    image_question VARCHAR2(40),
    CONSTRAINT fk_image_question FOREIGN KEY (image_question) REFERENCES Image(id_image)
);

CREATE TABLE Reponse (
    id_reponse VARCHAR2(40) PRIMARY KEY,
    texte_reponse VARCHAR2(32) NOT NULL,
    numero_reponse NUMBER(1) NOT NULL, --un indice représentant la réponse dans la question qui la réfèrence
    question_reponse NUMBER(4) NOT NULL, --la question à laquelle appartient la réponse
    CONSTRAINT fk_id_question FOREIGN KEY (question_reponse) REFERENCES Question(id_question)
);

CREATE TABLE Image (
    id_image VARCHAR2(40) PRIMARY KEY,
    nom_image TEXT,
    the_picture BLOB
);
CREATE TABLE Invitation (
    id_invitation VARCHAR2(40) PRIMARY KEY,
    groupe_invitation VARCHAR2(40) NOT NULL,
    eleve_invitation VARCHAR2(40) NOT NULL,
    CONSTRAINT fk_id_groupe FOREIGN KEY (groupe_invitation) REFERENCES Groupe(id_groupe),
    CONSTRAINT fk_id_eleve FOREIGN KEY (eleve_invitation) REFERENCES Eleve(id_eleve)
);
CREATE TABLE AssociationQuestionQuestionnaire (
    id_association_question_questionnaire VARCHAR2(40) PRIMARY KEY,
    questionnaire_association VARCHAR2(40) NOT NULL,
    question_association VARCHAR2(40) NOT NULL,
    CONSTRAINT fk_id_questionnaire FOREIGN KEY (questionnaire_association) REFERENCES Questionnaire(id_questionnaire),
    CONSTRAINT fk_id_question FOREIGN KEY (question_association) REFERENCES Question(id_question)
);

CREATE TABLE AssociationEleveAttaque (
    id_association_eleve_attaque VARCHAR2(40) PRIMARY KEY,
	type_association VARCHAR2(40) NOT NULL CHECK (type_association IN ("DEFENSEUR", "ATTAQUANT")),
    attaque_association VARCHAR2(40) NOT NULL,
    eleve_association VARCHAR2(40) NOT NULL,
    etat_attaque_association VARCHAR2(40) NOT NULL CHECK (etat_attaque_association IN ("ATTENTE", "ENCOURS", "FAIT")),
	points_association NUMBER,
    CONSTRAINT fk_id_attaque FOREIGN KEY (attaque_association) REFERENCES Attaque(id_attaque),
    CONSTRAINT fk_id_eleve FOREIGN KEY (eleve_association) REFERENCES Eleve(id_eleve)
);

CREATE TABLE AssociationThemeClasse (
    id_association_theme_classe VARCHAR(40) PRIMARY KEY,
    theme_association VARCHAR2(40) NOT NULL,
    classe_association VARCHAR2(40) NOT NULL,
    etat_association VARCHAR2(10) NOT NULL CHECK (etat_association IN ("MONTRER", "CACHER")),
    CONSTRAINT fk_id_theme FOREIGN KEY (theme_association) REFERENCES Theme(id_theme),
    CONSTRAINT fk_id_classe FOREIGN KEY (classe_association) REFERENCES Eleve(id_classe)
);

CREATE TABLE AssociationGroupeEleve (
    id_association_groupe_eleve VARCHAR(40) PRIMARY KEY,
    groupe_association VARCHAR2(40) NOT NULL,
    eleve_association VARCHAR2(40) NOT NULL,
    CONSTRAINT fk_id_groupe FOREIGN KEY (groupe_association) REFERENCES Groupe(id_groupe),
    CONSTRAINT fk_id_eleve FOREIGN KEY (eleve_association) REFERENCES Eleve(id_eleve)
);
COMMIT;
