const create = require("./procedure/create.js")
const get_all = require("./procedure/get_all.js")
const delete_all = require("./procedure/delete_all.js")

const db_schem = {
    Eleve: {
        primary_key:["id_eleve"],
        attributes:[
            "id_login_eleve",
            "mdp_eleve",
            "nom_eleve",
            "prenom_eleve",
            "points_eleve",
            "niveau_eleve",
            "classe_eleve"
        ]
    },
    Classe: {
        primary_key:["id_classe"],
        attributes:["nom_classe"]
    },
    Chateau: {
        primary_key:["id_chateau"],
        attributes:[
            "groupe_chateau",
            "nom_chateau"
        ]
    },
    Attaque: {
        primary_key:["id_attaque"],
        attributes:[
            "chateau_attaque",
            "groupe_attaque",
            "lance_attaque",
            "questionnaire_attaque"
        ]
    },
    Theme: {
        primary_key:["id_theme"],
        attributes:["nom_theme"]
    },
    Questionnaire:{
        primary_key:["id_questionnaire"],
        attributes:[
            "titre_questionnaire",
            "theme_questionnaire",
            "temps_questionnaire"
        ]
    },
    Question:{
        primary_key:["id_question"],
        attributes:[
            "titre_question",
            "type_question",
            "points_question",
            "bonne_reponse_question",
            "image_question"
        ]
    },
    Reponse:{
        primary_key:["id_reponse"],
        attributes:[
            "texte_reponse",
            "numero_reponse",
            "question_reponse"
        ]
    },
    Groupe:{
        primary_key:["id_groupe"],
        attributes:[
            "nom_groupe",
            "groupe_attaque",
            "chef_groupe"
        ]
    },
    Invitation:{
        primary_key:["id_invitation"],
        attributes:[
            "groupe_invitation",
            "eleve_invitation"
        ]
    },
    AssociationQuestionQuestionnaire:{
        primary_key:["id_association_question_questionnaire"],
        attributes:[
            "questionnaire_association",
            "question_association"
        ]
    },
    AssociationEleveAttaque:{
        primary_key:["id_association_eleve_attaque"],
        attributes:["type_association","attaque_association","eleve_association", "etat_attaque_association","points_association"]
    },
    AssociationThemeClasse:{
        primary:["id_association_theme_classe"],
        attributes:["theme_association","classe_association","etat_association"]
    },
    AssociationGroupeEleve:{
        primary:["id_association_groupe_eleve"],
        attributes:["groupe_association","eleve_association"]
    },
    Image:{
        primary_key:["id_image"],
        attributes:["nom_image","the_picture"]
    }
}

module.exports.schem = db_schem

module.exports.check_table = (table)=>{
    return new Promise((resolve,reject)=>{
        if(Object.keys(this.schem).includes(table)){
            resolve()
        }else{
            reject()
        }
    })
}

module.exports.check_attributes = (table,specified_attributes)=>{
    return new Promise((resolve,reject)=>{
        clean = true
        if(Array.isArray(attributes_names)){
            this.check_table(table).then(()=>{

                schem_attributes = db_schem[table].attributes
                specified_attributes.forEach(attribute=>{
                    if(!schem_attributes.includes(attribute)){
                        clean = false
                        reject({err:"no such attribute \""+attribute+"\" for the table "+table})
                    }
                })
                if(clean){
                    resolve()
                }

            }).catch((err)=>{
                if(err) reject(err)
                else reject({err:"no such table \""+table+"\""})
            })
        }else{
            reject({err:"second parameter must be an array"})
        }
    })
}

module.exports.fill = (clear,first_iteration,last_iteration)=>{
    return new Promise(async(resolve,reject)=>{
        if(clear){
            await new Promise((resolve,reject)=>{
                tables = Object.keys(db_schem)
                tables.forEach((table)=>{
                    delete_all.process({"table":table})
                    .then(console.log("fill > delete_all > "+table+" deleted"))
                    .catch((err)=>{
                        reject(err)
                    })
                })
                resolve()
            })
            .then()
            .catch((err)=>{
                reject(err)
            })
        }
        for(let i =0; i<6;i++){
            process_params= {
                table:"Chateau",
                attributes:[
                    {name:"nom_chateau",value:"Chateau"+i},
                    {name:"groupe_chateau",value:null}
                ]
            }
            await create.process(process_params)
            .then((id)=>{console.log("insertion chateau > ",id)})
            .catch((err)=>{console.log("insertion chateau > ",err);reject(err)})
        }
        for(let i = first_iteration; i<= last_iteration; i++){
            await fill_database(i)
            .then()
            .catch((err)=>{
                reject(err)
            })
        }
        resolve()
    })
}

function fill_database(index){
    return new Promise(async (resolve,reject)=>{

        let id_questionnaire = undefined
        let id_question = undefined
        let id_classe = undefined
        let id_theme = undefined
        let id_groupe = undefined
        let id_eleve = undefined

         let process_params = {
            table:"Classe",
            attributes:[
                 {name:"nom_classe",value:"ClasseQuiNeSerraPasInsere"}
            ]
        }
        await create.process(process_params)
        .then((id)=>{})
        .catch((err)=>{console.log("insertion classe > ",err);reject(err)})

        process_params = {
            table:"Classe",
            attributes:[
                {name:"nom_classe",value:"Classe"+index}
            ]
        }
        await create.process(process_params)
        .then((result)=>{id_classe = result.id;console.log("insertion classe > ",result,id_classe)})
        .catch((err)=>{console.log("insertion classe > ",err);reject(err)})

        process_params = {
            table:"Theme",
            attributes:[
                {name:"nom_theme",value:"Theme"+index}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_theme = id.id;console.log("insertion theme > ",id,id_theme)})
        .catch((err)=>{console.log("insertion theme > ",err);reject(err)})

        process_params = {
            table:"AssociationThemeClasse",
            attributes:[
                {name:"theme_association",value:id_theme},
                {name:"classe_association",value:id_classe},
                {name:"etat_association",value:"MONTRER"}
            ]
        }
        await create.process(process_params)
        .then((id)=>{console.log("insertion association_theme_classe > ",id)})
        .catch((err)=>{console.log("insertion association_theme_classe > ",err);reject(err)})

        process_params = {
            table:"Eleve",
            attributes:[
                {name:"id_login_eleve",value:"Eleve"+index},
                {name:"mdp_eleve",value:"EleveMDP"+index},
                {name:"nom_eleve",value:"EleveNom"+index},
                {name:"prenom_eleve",value:"ElevePrenom"+index},
                {name:"points_eleve",value:0},
                {name:"classe_eleve",value:id_classe}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_eleve = id.id;console.log("insertion eleve > ",id,id_eleve)})
        .catch((err)=>{console.log("insertion eleve > ",err);reject(err)})

        process_params = {
            table:"Eleve",
            attributes:[
                {name:"id_login_eleve",value:"Prof"+index},
                {name:"mdp_eleve",value:"ProfMDP"+index},
                {name:"nom_eleve",value:"ProfNom"+index},
                {name:"prenom_eleve",value:"ProfPrenom"+index},
                {name:"points_eleve",value:0},
                {name:"classe_eleve",value:"prof"}
            ]
        }
        await create.process(process_params)
        .then((id)=>{console.log("insertion prof > ",id)})
        .catch((err)=>{console.log("insertion prof > ",err);reject(err)})

        process_params = {
            table:"Questionnaire",
            attributes:[
                {name:"titre_questionnaire",value:"Questionnaire"+index},
                {name:"theme_questionnaire",value:id_theme},
                {name:"temps_questionnaire",value:300}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_questionnaire = id.id;console.log("insertion questionnaire > ",id,id_questionnaire)})
        .catch((err)=>{console.log("insertion questionnaire > ",err);reject(err)})

        process_params = {
            table:"Question",
            attributes:[
                {name:"titre_question",value:"Question"+index+"_1"},
                {name:"type_question",value:"QCM"},
                {name:"points_question",value:10},
                {name:"bonne_reponse_question",value:0},
                {name:"image_question",value:null}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_question = id.id;console.log("insertion question > ",id,id_question)})
        .catch((err)=>{console.log("insertion question > ",err);reject(err)})

        process_params = {
            table:"Reponse",
            attributes:[
                {name:"texte_reponse",value:"Reponse"+index+"_1_1"},
                {name:"numero_reponse",value:0},
                {name:"question_reponse",value:id_question},
            ]
        }
        await create.process(process_params)
        .then((id)=>{console.log("insertion reponse > ",id)})
        .catch((err)=>{console.log("insertion reponse > ",err);reject(err)})

        process_params = {
            table:"Reponse",
            attributes:[
                {name:"texte_reponse",value:("Reponse"+index+"_1_2")},
                {name:"numero_reponse",value:1},
                {name:"question_reponse",value:id_question},
            ]
        }
        await create.process(process_params)
        .then((id)=>{console.log("insertion reponse > ",id)})
        .catch((err)=>{console.log("insertion reponse > ",err);reject(err)})

        process_params = {
            table:"Groupe",
            attributes:[
                {name:"nom_groupe",value:"Groupe"+index},
                {name:"chef_groupe",value:id_eleve}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_groupe = id.id;console.log("insertion groupe > ",id,id_groupe)})
        .catch((err)=>{console.log("insertion groupe > ",err);reject(err)})

        

        // process_params = {
        //     table:"AssociationGroupeEleve",
        //     attributes:[
        //         {name:"groupe_association",value:id_groupe},
        //         {name:"eleve_association",value:id_eleve},
        //     ]
        // }
        // await create.process(process_params)
        // .then(id=>console.log("insertion groupe_eleve > ",id))
        // .catch((err)=>{console.log("insertion groupe_eleve > ",err);reject(err)})
        
        // process_params = {
        //     table:"Invitation",
        //     attributes:[
        //         {name:"groupe_invitation",value:id_groupe},
        //         {name:"eleve_invitation",value:id_eleve}
        //     ]
        // }
        // await create.process(process_params)
        // .then((id)=>{console.log("insertion invitation > ",id)})
        // .catch((err)=>{reject(err)})

        process_params = {
            table:"AssociationQuestionQuestionnaire",
            attributes:[
                {name:"questionnaire_association",value:id_questionnaire},
                {name:"question_association",value:id_question},
            ]
        }
        await create.process(process_params)
        .then(id=>console.log("insertion association_question_questionnaire > ",id))
        .catch((err)=>{console.log("insertion association_question_questionnaire > ",err);reject(err)})

        process_params = {
            table:"Question",
            attributes:[
                {name:"titre_question",value:"Question"+index+"_2"},
                {name:"type_question",value:"OUVERTE"},
                {name:"points_question",value:10},
                {name:"bonne_reponse_question",value:0},
                {name:"image_question",value:null}
            ]
        }
        await create.process(process_params)
        .then((id)=>{id_question = id.id;console.log("insertion question > ",id,id_question)})
        .catch((err)=>{console.log("insertion question > ",err);reject(err)})

        process_params = {
            table:"Reponse",
            attributes:[
                {name:"texte_reponse",value:"Reponse"+index+"_2_1"},
                {name:"numero_reponse",value:0},
                {name:"question_reponse",value:id_question},
            ]
        }
        await create.process(process_params)
        .then((id)=>{console.log("insertion reponse > ",id)})
        .catch((err)=>{console.log("insertion reponse > ",err);reject(err)})

        process_params = {
            table:"AssociationQuestionQuestionnaire",
            attributes:[
                {name:"questionnaire_association",value:id_questionnaire},
                {name:"question_association",value:id_question},
            ]
        }
        await create.process(process_params)
        .then(id=>{console.log("insertion association_question_questionnaire > ",id);resolve()})
        .catch((err)=>{console.log("insertion association_question_questionnaire > ",err);reject(err)})       


    })
}

//print all the rows in each tables of the database, used to check the result of the precedent action
module.exports.check_databases = ()=>{
    return new Promise((resolve,reject)=>{
        Object.keys(db_schem).forEach(async(table)=>{
            await get_all.process({"table":table})
            .then((result)=>{console.log("get_all > "+table+" :",result)})
            .catch((err)=>{
                reject(err)
            })
        });
        resolve()
    })
}
