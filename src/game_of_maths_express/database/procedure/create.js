const db = require("../db.js")
const db_utils = require("../db_utils.js")

module.exports.needed_fields = ["table","attributes"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db_utils.check_table(request.table).then(()=>{
            if(Array.isArray(request.attributes)){
                attributes_names = []
                attributes_values = []
                request.attributes.forEach(attribute => {
                    if(attribute.name !== undefined){
                        attributes_names.push(attribute.name)
                    }else{
                        reject({err:"each attribute in attribute's field must have a \"name\" attribute"})
                    }
                    if(attribute.value !== undefined){
                        attributes_values.push(attribute.value)
                    }else{
                        reject({err:"each attribute in attribute's field must have a \"value\" attribute"})
                    }
                });
                db_utils.check_attributes(request.table,attributes_names)
                .then(()=>{
                    const { v4: uuidv4 } = require('uuid');
                    new_id = uuidv4();
                    sqlStatement = "INSERT INTO "+request.table+" values ('"+new_id+"', "
                    attributes_names.forEach((name)=>{
                        sqlStatement = sqlStatement+" ? "
                        if(!(name === attributes_names[attributes_names.length-1])){
                            sqlStatement = sqlStatement+", "
                        }
                    })
                    sqlStatement = sqlStatement + ")"
                    db/*.run("SELECT * FROM "+request.table+" where "+db_utils.schem[request.table].primary_key+" =  ?",[new_id])*/
                    .run(sqlStatement,attributes_values,(err)=>{
                        if(err != null) {reject(err) }
                        else resolve({id:new_id})
                    })
                })
                .catch((err)=>{
                    reject(err)
                })
            }else{
                reject({err:"attributes's field of the request must be an array"})
            }
        }).catch(()=>{
            reject({err:"no table \""+request.table+"\" found "})
        })
    })
}