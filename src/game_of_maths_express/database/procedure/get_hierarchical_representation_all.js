module.exports.needed_fields = []
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        generate_hierarchical_representation()
        .then((representation)=>resolve(representation))
        .catch((err)=>reject(err))
    })
}

function generate_hierarchical_representation(data){
    return new Promise(function(resolve,reject){
        get_data().then((data)=>{
            let questionnaires = data.questionnaires
            let questions = data.questions
            let reponses = data.reponses
            let associations = data.associations
            let representations = []
            questionnaires.forEach(questionnaire => {
                let representation = {};
                representation.questionnaire = questionnaire;
                let questions_id = []                    
                
                associations.forEach(association =>{
                    if(association.questionnaire_association === representation.questionnaire.id_questionnaire){
                        questions_id.push(association.question_association);
                    }
                })
    
                representation.questionnaire.questions = [];
                questions.forEach(question => {
                    if(questions_id.includes(question.id_question)){
                        question.reponses = [];
                        representation.questionnaire.questions.push(question);                        
                    }
                });
    
                reponses.forEach(reponse => {
                    if(questions_id.includes(reponse.question_reponse)){
                        questions.forEach(question => {
                            if(reponse.question_reponse === question.id_question)
                            question.reponses.push(reponse);
                        })
                    }
                })
                representations.push(representation);
    
            });
            resolve(representations)
        }).catch((err)=>{
            reject(err)
        })
    })
}

function get_data(){
    return new Promise(function(resolve,reject){
        obj_to_resolve = {
            questionnaires: [],
            questions: [],
            reponses: [],
            associations: []
        }
        get_all = require("./get_all")
        get_all.process({table:"Questionnaire"})
        .then((questionnaires)=>{
            obj_to_resolve.questionnaires = questionnaires
            get_all.process({table:"Question"})
            .then((questions)=>{
                obj_to_resolve.questions = questions
                get_all.process({table:"Reponse"})
                .then((reponses)=>{
                    obj_to_resolve.reponses = reponses
                    get_all.process({table:"AssociationQuestionQuestionnaire"})
                    .then((associations)=>{
                        obj_to_resolve.associations = associations
                        resolve(obj_to_resolve)
                    })
                    .catch((err)=>{
                        reject(err)
                    })
                })
                .catch((err)=>{
                    reject(err)
                })
            })
            .catch((err)=>{
                reject(err)
            })
        })
        .catch((err)=>{
            reject(err)
        })
    })
}