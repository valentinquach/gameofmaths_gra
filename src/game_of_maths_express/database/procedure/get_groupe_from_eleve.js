const db = require('../db.js');
const get_one = require('./get_one')

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){

        get_one.process({table:'Eleve',pk:request.pk})
        .then((eleve)=>{
            if(eleve.classe_eleve === 'prof'){
                let rows_prof = []
                resolve(rows_prof)

            }else{

                sql_statement = "SELECT * FROM Groupe WHERE id_groupe IN"
                sql_statement = sql_statement + "(SELECT groupe_association FROM AssociationGroupeEleve WHERE eleve_association = ?)"
                db.get(sql_statement,[request.pk],(err,rows)=>{
                    if(err) reject(err)
                    else{
                        sql_statement="SELECT * FROM Groupe WHERE chef_groupe = ?"
                        db.get(sql_statement,[request.pk],(err,row)=>{
                            if (err) {
                                reject(err)
                            }
                            else if (row != undefined) {
                                resolve(row)
                            }
                            else if(rows != undefined){
                                resolve(rows)
                            }else{
                                resolve([])
                            }
                        })
                    }
                })

            }
        })
        .catch((err)=>reject(err))
    })
}
