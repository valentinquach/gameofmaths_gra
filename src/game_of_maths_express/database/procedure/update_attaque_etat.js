const db = require('../db')
const update = require('./update')

module.exports.needed_fields = ['pk_attaque','pk_eleve','state','score']
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        let sql_statement = "SELECT * FROM AssociationEleveAttaque WHERE attaque_association = ? AND eleve_association = ?"
        db.get(sql_statement,[request.pk_attaque,request.pk_eleve],(err,row)=>{
            if(err) reject(err)
            else{
                let data = [{name:"etat_attaque_association",value:request.state}]
                if(request.score > -1){
                    data.push({name:"points_association",value:request.score})
                }
                update.process({
                    table:"AssociationEleveAttaque",
                    pk:row.id_association_eleve_attaque,
                    attributes:data
                })
                .then((result)=>{
                    resolve({result:"sucess"})
                })
                .catch((err)=>{erject(err)})
            }
        })
    })
}