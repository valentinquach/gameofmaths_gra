const db = require('../db.js')
const get_questionnaires = require('./get_questionnaire_from_theme')
const delete_questionnaire = require('./delete_children')
const delete_one = require('./delete_one')

module.exports.needed_fields = ['pk']
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        delete_associations(request.pk)
        .then(()=>{
            delete_questionnaires(request.pk)
            .then(()=>{
                delete_one.process({table:"Theme",pk:request.pk})
                .then(()=>{
                    resolve({result:"success"})
                })
                .catch((err)=>reject(err))
            })
            .catch((err)=>reject(err))
        })
        .catch((err)=>reject(err))
    })
}

function delete_questionnaires(id){
    return new Promise((resolve,reject)=>{
        get_questionnaires.process({pk:id})
        .then((questionnaires)=>{
            Promise.all(questionnaires.map((questionnaire)=>{
                return new Promise((resolve,reject)=>{
                    delete_questionnaire.process({table:'Questionnaire',pk:questionnaire.id_questionnaire})
                    .then(()=>{
                        resolve()
                    })
                    .catch((err)=>reject(err))
                })
            }))
            .then(()=>{resolve()})
            .catch((err)=>reject(err))
        })
        .catch((err)=>reject(err))
    })
}

function delete_associations(id){
    return new Promise((resolve,reject)=>{
        console.log("bruh0-1")
        sql_statement = "DELETE FROM AssociationThemeClasse WHERE theme_association = ?"
        db.run(sql_statement,[id],(err)=>{
            if(err){reject(err)}
            else{
                resolve()
            }
        })
    })
}