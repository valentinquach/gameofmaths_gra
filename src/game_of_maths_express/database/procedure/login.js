module.exports.needed_fields = ["id","mdp"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db_utils = require("../db_utils.js")
        db = require('../db.js');
        db.get("SELECT * FROM Eleve WHERE id_login_eleve = ? AND mdp_eleve = ?",[request.id,request.mdp],(err,row)=>{
            if(err) reject(err)
            else if(row){
                resolve(row)
            }
            else reject({err:"mauvais identifiant ou mot de passe"})
        })
    })
}