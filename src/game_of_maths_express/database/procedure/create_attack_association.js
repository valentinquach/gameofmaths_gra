const db = require('../db')
const create = require('./create')
const get_one = require('./get_one')
const get_eleves = require('./get_eleves_from_groupe')

module.exports.needed_fields = ['pk']

module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        get_one.process({table:'Attaque',pk:request.pk})
        .then((attaque)=>{
            get_one.process({table:'Chateau',pk:attaque.chateau_attaque})
            .then((chateau)=>{
                create_associations(chateau.groupe_chateau,chateau,attaque)
                .then(()=>{
                    create_associations(attaque.groupe_attaque,chateau,attaque)
                    .then(()=>{
                        resolve({result:"succes"})
                    })
                    .catch((err)=>{reject(err)})
                })
                .catch((err)=>{reject(err)})
            })
            .catch((err)=>{reject(err)})
        })
        .catch((err)=>{reject(err)})
    })
}

function create_associations(pk_groupe,chateau,attaque){
    return new Promise((resolve,reject)=>{        
        get_one.process({table:"Groupe",pk:pk_groupe})
        .then((groupe)=>{
            get_eleves.process({pk_groupe:groupe.id_groupe,pk_chef:groupe.chef_groupe})
            .then((menbers)=>{
                let type = undefined
                if(pk_groupe === chateau.groupe_chateau) type = "DEFENSEUR"
                else type = "ATTAQUANT"
                create.process({table:'AssociationEleveAttaque',attributes:[
                    {name:"type_association",value:type},
                    {name:"attaque_association",value:attaque.id_attaque},
                    {name:"eleve_association",value:menbers.chef.id_eleve},
                    {name:"etat_attaque_association",value:"ATTENTE"},
                    {name:"points_association",value:0},
                ]})
                .then(()=>{
                    Promise.all(menbers.eleves.map(eleve=>{
                        return new Promise((resolve,reject)=>{
                            create.process({table:'AssociationEleveAttaque',attributes:[
                                {name:"type_association",value:type},
                                {name:"attaque_association",value:attaque.id_attaque},
                                {name:"eleve_association",value:eleve.id_eleve},
                                {name:"etat_attaque_association",value:"ATTENTE"},
                                {name:"points_association",value:0},
                            ]})
                            .then(()=>{resolve()})
                            .catch((err)=>{reject(err)})
                        })
                    }))
                    .then(()=>{resolve()}) //sortie
                    .catch((err)=>{reject(err)})
                })
                .catch((err)=>{reject(err)})
            })
            .catch((err)=>{reject(err)})
        })
        .catch((err)=>{reject(err)})
    })
}