const create = require("./create.js")
const db_utils = require("../db_utils.js")

module.exports.needed_fields = ["pk","attributes"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        request["table"] = "Question"
        create.process(request)
        .then((result)=>{
            create.process({
                "table":"AssociationQuestionQuestionnaire",
                attributes:[
                    {name:"questionnaire_association",value:request.pk},
                    {name:"question_association",value:result.id}
                ]
            })
            .then(()=>{resolve({result:"success"})})
            .catch((err)=>{reject(err)})

        })
        .catch((err)=>{reject(err)})
    })
}