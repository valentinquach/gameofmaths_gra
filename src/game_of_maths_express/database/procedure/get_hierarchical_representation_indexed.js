const db = require('../db')
const generate = require('./get_hierarchical_representation_one')

module.exports.needed_fields = ["index","gap"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db.all("SELECT id_questionnaire FROM Questionnaire WHERE rowid >= ? and rowid < ?",[request.index,request.index+request.gap],(err,rows)=>{
            if(err) reject(err)
            else{
                db.get("SELECT COUNT(id_questionnaire) AS item_number FROM Questionnaire",(err,row)=>{
                    if(err) reject(err)
                    else {
                        let representation_result = {item_number:row.item_number,data:[]}
                        Promise.all(rows.map((questionnaire)=>{
                            return new Promise((resolve,reject)=>{
                                generate.process({pk:questionnaire.id_questionnaire})
                                .then((questionnaire_representation)=>{
                                    representation_result.data.push({questionnaire:questionnaire_representation})
                                    resolve()
                                })
                                .catch((err)=>{reject(err)})
                            })
                        }))
                        .then(()=>{
                            resolve(representation_result)
                        })
                        .catch((err)=>{
                            reject(err)
                        })                        
                    }
                })
            }
        })
    })
}