const db = require('../db.js');
const get_one = require('./get_one.js')

module.exports.needed_fields = ["pk_groupe","pk_chef"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        sql_statement = "SELECT * FROM Eleve WHERE id_eleve IN"
        sql_statement = sql_statement + "(SELECT eleve_association FROM AssociationGroupeEleve WHERE groupe_association = ?)"
        db.all(sql_statement,[request.pk_groupe],(err,rows)=>{
            if(err) reject(err)
            else{
                get_one.process({table:"Eleve",pk:request.pk_chef})
                .then((eleve)=>{
                    resolve({chef:eleve,eleves:rows})
                })
                .catch((err)=>reject(err))
            }
        })
    })
}
