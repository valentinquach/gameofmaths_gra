const db = require('../db')

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        generate_hierarchical_representation(request.pk)
        .then((representation)=>resolve(representation))
        .catch((err)=>reject(err))
    })
}

function generate_hierarchical_representation(id_questionnaire){
    return new Promise((resolve,reject)=>{
        get_one = require('./get_one')
        get_one.process({table:"Questionnaire","pk":id_questionnaire}).then((questionnaire)=>{
            get_questions_data(questionnaire.id_questionnaire)
            .then((questions)=>{
                questionnaire.questions = questions

                Promise.all(
                    questions.map((question)=>{
                        return new Promise((resolve,reject)=>{
                            get_reponses_data(question.id_question)
                            .then((reponses)=>{
                                question.reponses = reponses
                                resolve()
                            })
                            .catch((err)=>reject(err))
                        })
                    })
                )
                .then(()=>{
                    db.get("SELECT * FROM Theme WHERE id_theme = ?",[questionnaire.theme_questionnaire],(err,row)=>{
                        if(err) reject(err)
                        else{
                            if(row != undefined){
                                questionnaire["nom_theme_questionnaire"] = row.nom_theme
                            }else{
                                questionnaire["nom_theme_questionnaire"] = "theme not found"
                            }
                            resolve(questionnaire)
                        }
                    })
                    // resolve(questionnaire)
                })
                .catch((err)=>reject(err))

            })
            .catch((err)=>reject(err))
        })
        .catch((err)=>reject(err))
    })
}

function get_questions_data(id_questionnaire){
    return new Promise((resolve,reject)=>{
        db.all("SELECT * FROM Question WHERE id_question IN (SELECT question_association FROM AssociationQuestionQuestionnaire WHERE questionnaire_association = ? )",[id_questionnaire],(err,rows)=>{
            if(err) reject(err)
            else resolve(rows)
        })
    })
}

function get_reponses_data(id_question){
    return new Promise((resolve,reject)=>{
        db.all("SELECT * FROM Reponse WHERE question_reponse = ?",[id_question],(err,rows)=>{
            if(err) reject(err)
            else resolve(rows)
        })
    })
}