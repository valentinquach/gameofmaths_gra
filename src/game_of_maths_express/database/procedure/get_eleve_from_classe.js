module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        sql_statement = "SELECT * FROM Eleve WHERE classe_eleve = ?"
        db = require('../db.js');
        db.all(sql_statement,[request.pk],(err,rows)=>{
            if(err) reject(err)
            else resolve(rows)
        })
    })
}
