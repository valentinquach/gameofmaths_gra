const get_one = require('./get_one')
const update = require('./update')
const get_groupe = require('./get_groupe_from_eleve')
const get_asso = require('./get_attaque_association_all')
const delete_one = require('./delete_one')
const delete_attaque_childrens = require('./delete_children_attaque')

module.exports.needed_fields = ['pk'] //id_attaque
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        get_one.process({table:"Attaque",pk:request.pk})
        .then((attaque)=>{
            get_one.process({table:"Chateau",pk:attaque.chateau_attaque})
            .then((chateau)=>{
                get_asso.process({pk:request.pk})
                .then((assos)=>{
                    let score_def = 0
                    assos.defenseurs.forEach(defenseur => {
                        let coef = 1
                        if(defenseur.eleve.points_eleve > 0) coef = defenseur.eleve.points_eleve > 0
                        score_def = score_def + (defenseur.points_association*coef)
                    });
                    let score_att = 0
                    assos.attaquants.forEach(attaquant => {
                        let coef = 1
                        if(attaquant.eleve.points_eleve > 0) coef = attaquant.eleve.points_eleve > 0
                        score_att = score_att + (attaquant.points_association*coef)
                    });

                    delete_attaque_childrens.process({pk:request.pk})
                    .then(()=>{
                        delete_one.process({table:"Attaque",pk:request.pk})
                        .then(()=>{

                            if(score_att > score_def){
                                change_chateau_groupe(assos.attaquants[0].eleve.id_eleve,chateau)
                                resolve({result:"attaquants win"})
                            }else if(score_att == score_def){
                                if(assos.attaquants < assos.defenseurs){
                                    change_chateau_groupe(assos.attaquants[0].eleve.id_eleve,chateau)
                                    resolve({result:"attaquants win"})
                                }else{
                                    resolve({result:"defenseurs win"})
                                }
                            }else{
                                resolve({result:"defenseurs win"})
                            }

                        })
                        .catch((err)=>reject(err))
                    })
                    .catch((err)=>reject(err))
                })
                .catch((err)=>{reject(err)})
            })
            .catch((err)=>{reject(err)})
        })
        .catch((err)=>{reject(err)})
    })
}

function change_chateau_groupe(id_eleve,chateau){
    return new Promise((resolve,reject)=>{
        get_groupe.process({pk:id_eleve})
        .then((groupe)=>{
            update.process({table:"Chateau",pk:chateau.id_chateau,attributes:[{name:"groupe_chateau",value:groupe.id_groupe}]})
            .then(()=>{
                resolve({result:"attaquants win"})
            }).catch((err)=>reject(err))

        })
        .catch((err)=>{reject(err)})
    })
}