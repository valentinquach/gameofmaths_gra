const get_one = require("./get_one.js")
const get_eleves = require('./get_eleves_from_groupe')
const get_attaque = require('./get_attaque_from_chateau')
const db = require("../db.js")

module.exports.needed_fields = []
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        db.all("SELECT * FROM Chateau",(err,rows)=>{
          if(err) reject(err)
            else{
                Promise.all(
                    rows.map((row)=>{ //row -> un chateau
                        return new Promise((resolve,reject)=>{
                            get_one.process({table:"Groupe",pk:row.groupe_chateau})
                            .then((groupe)=>{
                                row['groupe'] = groupe
                                console.log("groupe => ",groupe)
                                get_eleves.process({pk_groupe:groupe.id_groupe,pk_chef:groupe.chef_groupe})
                                .then((eleves)=>{
                                    row.groupe['eleves'] = eleves

                                    get_attaque.process({pk:row.id_chateau})
                                    .then((result)=>{
                                        if(result.attaque != undefined){
                                            //Attaque trouvé
                                            row['attaque'] = result.attaque
                                            get_one.process({table:"Groupe",pk:result.attaque.groupe_attaque})
                                            .then((groupe_attaque)=>{
                                                row.attaque['groupe'] = groupe_attaque
                                                console.log("groupe_att =>",groupe_attaque)
                                                get_eleves.process({pk_groupe:groupe_attaque.id_groupe,pk_chef:groupe_attaque.chef_groupe})
                                                .then((eleves_attaque)=>{
                                                    row.attaque.groupe['eleves'] = eleves_attaque
                                                    resolve()
                                                })
                                                .catch((err)=>{
                                                    console.log("get_chateau_all > get_eleves_attaque > ",err)
                                                    reject(err)
                                                })
                                            })
                                            .catch((err)=>{
                                                console.log("get_chateau_all > get_groupe_attaque > ",err)
                                                reject(err)})
                                        }else{
                                            //pas d'attaque en cours
                                            row['attaque'] = undefined
                                            resolve()
                                        }
                                    })
                                    .catch((err)=>{
                                        console.log("get_chateau_all > get_attaque > ",err)
                                        reject(err)
                                    })

                                })
                                .catch((err)=>{
                                    console.log("get_chateau_all > get_eleves > ",err)
                                    reject(err)
                                })
                            })
                            .catch((err)=>{
                                console.log("get_chateau_all > get_groupe > ",err)
                                row['groupe'] = undefined
                                resolve()
                            })
                        })
                    })
                )
              .then(()=>{resolve(rows)}) //then du Promise.all
              .catch((err)=>reject(err))
            }
        })
    })
}