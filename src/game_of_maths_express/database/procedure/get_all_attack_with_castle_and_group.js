const get_one = require("./get_one.js")
const db = require("../db.js")

module.exports.needed_fields = []
module.exports.process = function(result){
    return new Promise((resolve,reject)=>{
        db.all("SELECT * FROM Attaque",(err,rows)=>{
            if(err) reject(err)
            else{
                Promise.all(
                rows.map((row)=>{ //row -> un chateau
                    return new Promise((resolve,reject)=>{
                        get_one.process({table:"Chateau",pk:row.chateau_attaque})
                        .then((castle)=>{
                            row["castle"] = castle
                            get_one.process({table:"Groupe",pk:row.groupe_attaque})
                            .then((groupe)=>{
                                row["groupe"] = groupe
                                resolve()
                            }).catch((err)=>reject(err))
                        }).catch((err)=>reject(err))  
                        
                        .catch((err)=>reject(err))
                        })
                    })
                )
                .then(()=>{resolve(rows)}) //then du Promise.all
                .catch((err)=>reject(err))
            }
        })
    })
}