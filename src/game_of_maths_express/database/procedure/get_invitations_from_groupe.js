const db = require('../db.js');
const get_one = require('./get_one')

module.exports.needed_fields = ["groupe"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        sql_statement = "SELECT * FROM Invitation WHERE groupe_invitation = ?"
        db.all(sql_statement,[request.groupe],(err,rows)=>{
            if(err) reject(err)
            else{
                Promise.all(rows.map((row)=>{
                    return new Promise((resolve,reject)=>{
                        get_one.process({table:'Eleve',pk:row.eleve_invitation})
                        .then((eleve)=>{
                            row['eleve'] = eleve
                            resolve()
                        })
                        .catch((err)=>{reject(err)})
                    })
                }))
                .then(()=>{resolve(rows)})
                .catch((err)=>{reject(err)})
            }
        })
    })
}
