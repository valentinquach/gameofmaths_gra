module.exports.needed_fields = ["table","pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db_utils = require("../db_utils.js")
        db_utils.check_table(request.table).then(()=>{
            db = require('../db.js');
            db.run("DELETE FROM "+request.table+" where "+db_utils.schem[request.table].primary_key+" = ? ",[request.pk],(err)=>{
                if(err) reject(err)
                else resolve("success")
            })
        }).catch(()=>{
            reject({err:"no such table \""+request.table+"\""})
        })
    })
}