const db = require("../db.js")
const delete_one = require("./delete_one.js")

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        sql_statement = "DELETE FROM AssociationEleveAttaque WHERE attaque_association = ?"
        db.run(sql_statement,[request.pk],(err)=>{
            if(err) reject(err)
            else {
                delete_one.process({table:"Attaque",pk:request.pk})
                .then(()=>resolve("success"))
                .catch((err)=>reject(err))
            }
        })
    })
}
