const db = require('../db.js')

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        sql_statement = "SELECT * FROM Theme WHERE id_theme IN"
        sql_statement = sql_statement + "(SELECT theme_association FROM AssociationThemeClasse WHERE classe_association = ?)"
        db.all(sql_statement,[request.pk],(err,rows)=>{
            if(err) reject(err)
            else{
                let themes_result = []
                sql_statement = "SELECT * FROM AssociationThemeClasse WHERE classe_association = ? AND theme_association = ?"
                Promise.all(rows.map((theme)=>{
                    return new Promise((resolve,reject)=>{
                        db.get(sql_statement,[request.pk,theme.id_theme],(err,row)=>{
                            if(err) reject(err)
                            else{
                                theme["show_theme"] = (row.etat_association === 'MONTRER')
                                themes_result.push(theme)
                                resolve()
                            }
                        })
                    })
                }))
                .then(()=>{resolve(themes_result )})
                .catch((err)=>reject(err))
            }
        })
    })
}