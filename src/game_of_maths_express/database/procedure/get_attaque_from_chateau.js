const db = require('../db')

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        sql_statement = "SELECT * FROM Attaque WHERE chateau_attaque = ?"
        db.get(sql_statement,[request.pk],(err,row)=>{
            if(err) reject(err)
            else{
                if(row != undefined){
                    resolve({attaque:row})
                }else{
                    resolve({attaque:undefined})
                }
            }
        })
    })
}