const db = require('../db.js');

module.exports.needed_fields = ["pk_eleve","pk_groupe"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        sql_statement = "SELECT * FROM Invitation WHERE eleve_invitation = ? AND groupe_invitation = ?"
        db.get(sql_statement,[request.pk_eleve,request.pk_groupe],(err,row)=>{
            if(err) reject(err)
            else{
                resolve(row !== undefined)
            }
        })
    })
}