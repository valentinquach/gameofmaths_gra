module.exports.needed_fields = ["table","pk"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db_utils = require("../db_utils.js")
        db_utils.check_table(request.table)
        .then(()=>{
            db = require('../db.js');
            db.get("SELECT * FROM "+request.table+" where "+db_utils.schem[request.table].primary_key+" = ? ",[request.pk],(err,row)=>{
                if(err){
                    reject(err)
                }
                else if(row){
                    resolve(row)
                }
                else {
                    reject({err:"no "+request.table+" found with the primary key \""+request.pk+"\""})
                }
            })
        })
        .catch((err)=>{
            if(err) reject(err)
            else reject({err:"no such table \""+request.table+"\""})
        })
    })
}