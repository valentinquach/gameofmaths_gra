const delete_one = require("./delete_one.js")
const db = require("../db.js")
const db_utils = require("../db_utils.js")

module.exports.needed_fields = ["table","pk"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        if(request.table === "Questionnaire"){
            delete_questionnaire_children(request.pk)
            .then(()=>{
                delete_one.process({table:"Questionnaire","pk":request.pk})
                .then(()=>{resolve("success")})
                .catch((err)=>reject(err))
            })
            .catch((err)=>reject(err))
        }else if(request.table === "Question"){
            delete_question_children(request.pk)
            .then(()=>{
                delete_one.process({table:"Question",pk:request.pk})
                .then(()=>{resolve("success")})
                .catch((err)=>reject(err))
            })
            .catch((err)=>reject(err))
        }else if(request.table === "Reponse"){
            delete_one.process({table:"Reponse",pk:request.pk})
            .then(()=>{resolve("success")})
            .catch((err)=>reject(err))
        }else{
            reject({err:" \""+requet.table+"\" is not a valid table for this procedure"})
        }
    })
}

//delete questions of the specified questionnaire
//who aren't associated with another questionnaire
function delete_questionnaire_children(id){
    return new Promise((resolve,reject)=>{
        sql_statement = "SELECT question_association FROM AssociationQuestionQuestionnaire WHERE questionnaire_association = ? AND question_association NOT IN"
        sql_statement = sql_statement+"( SELECT question_association FROM AssociationQuestionQuestionnaire WHERE questionnaire_association != ? )"
        db.all(sql_statement,[id,id],(err,rows)=>{
            if(err) reject(err)
            else{
                Promise.all(
                    rows.map((row)=>{delete_question_children(row.id_question)})
                ).then(()=>{
                    sql_statement = "DELETE FROM AssociationQuestionQuestionnaire WHERE question_association = ? AND questionnaire_association = ?"

                    Promise.all(
                        rows.map((row)=>{
                            return new Promise((resolve,reject)=>{
                                db.run(sql_statement,[row.id_question,id],(err)=>{
                                    if(err) reject(err)
                                    else{
                                        resolve()
                                    }
                                })
                            })
                        })
                    ).then(()=>{
                        Promise.all(
                            rows.map((row)=>{delete_one.process({table:"Question",pk:row.id_question})})
                        ).then(()=>{
                            resolve()
                        })
                        .catch((err)=>reject(err))
                    })
                    .catch((err)=>{reject(err)})

                })
                .catch((err)=>reject(err))
            }
        })
    })
}

//delete reponse of the specifeid question
function delete_question_children(id){
    return new Promise((resolve,reject)=>{
        db.run("DELETE FROM Reponse WHERE id_reponse = ?",[id],(err)=>{
            if(err) reject(err)
            else resolve()
        })
    })
}