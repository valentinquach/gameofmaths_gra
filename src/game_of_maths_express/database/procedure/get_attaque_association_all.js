const db = require('../db')
const get_one = require('./get_one')

module.exports.needed_fields = ['pk']
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        let sql_statement = "SELECT * FROM AssociationEleveAttaque WHERE attaque_association = ? AND type_association = ?"
        db.all(sql_statement,[request.pk,"DEFENSEUR"],(err,defenseurs_asso)=>{
            if(err) reject(err)
            else{
                let result_to_resolve = {}
                result_to_resolve.defenseurs = defenseurs_asso
                Promise.all(defenseurs_asso.map((asso)=>{
                    return new Promise((resolve,reject)=>{
                        get_one.process({table:"Eleve",pk:asso.eleve_association})
                        .then((defenseur)=>{
                            asso.eleve = defenseur
                            resolve()
                        })
                        .catch((err)=>{reject(err)})
                    })
                }))
                .then(()=>{ //Promise.all then
                    db.all(sql_statement,[request.pk,"ATTAQUANT"],(err,attaquants_asso)=>{
                        if(err) reject(err)
                        else{
                            result_to_resolve.attaquants = attaquants_asso
                            Promise.all(attaquants_asso.map((asso)=>{
                                return new Promise((resolve,reject)=>{
                                    get_one.process({table:"Eleve",pk:asso.eleve_association})
                                    .then((attaquant)=>{
                                        asso.eleve = attaquant
                                        resolve()
                                    })
                                    .catch((err)=>{reject(err)})
                                })
                            }))
                            .then(()=>{ //Promise.all then
                                resolve(result_to_resolve)
                            })
                            .catch((err)=>{reject(err)})
                        }
                    })

                })
                .catch((err)=>{reject(err)})
            }
        })
    })
}