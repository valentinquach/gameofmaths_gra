module.exports.needed_fields = ["table"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db_utils = require("../db_utils.js")
        db_utils.check_table(request.table).then(()=>{         
            db = require('../db.js');
            db.all("DELETE FROM "+request.table,(err,rows)=>{
                if(err) reject(err)
                else resolve(rows)
            })
        }).catch(()=>{
            reject({err:"no such table \""+request.table+"\""})
        })
    })
}