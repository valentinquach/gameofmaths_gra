const db = require('../db')
const get_one = require('./get_one')

module.exports.needed_fields = ['pk_attaque','pk_eleve']
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        let sql_statement = "SELECT * FROM AssociationEleveAttaque WHERE attaque_association = ? AND eleve_association = ?"
        db.get(sql_statement,[request.pk_attaque,request.pk_eleve],(err,row)=>{
            if(err) reject(err)
            else{
                resolve(row)
            }
        })
    })
}