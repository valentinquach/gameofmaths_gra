//get indexed eleve data without getting prof profile
const db = require('../db')
const db_utils = require('../db_utils')
const generate = require('./get_one')

module.exports.needed_fields = ["index","gap"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db.all("SELECT * FROM Eleve WHERE rowid >= ? and rowid < ? AND classe_eleve != ?",[request.index,request.index+request.gap,"prof"],(err,rows)=>{
            if(err) reject(err)
            else{
                db.get("SELECT COUNT(id_eleve) AS item_number FROM Eleve WHERE classe_eleve != ?",["prof"],(err,row)=>{
                    if(err) reject(err)
                    else {
                        let representation_result = {item_number:row.item_number,data:[]}

                        Promise.all(
                            rows.map(eleve=>{
                                return new Promise((resolve,reject)=>{
                                    generate.process({table:"Classe",pk:eleve.classe_eleve})
                                    .then((classe)=>{
                                        eleve.classe = classe
                                        resolve()
                                    })
                                    .catch((err)=>reject(err))
                                })
                            })
                        )
                        .then(()=>{
                            representation_result.data=rows
                            console.log("representation",representation_result)
                            resolve(representation_result)
                        })           
                        .catch((err)=>reject(err))
                    }
                })
            }
        })
    })
}