const db = require('../db')
const db_utils = require('../db_utils')

module.exports.needed_fields = ["table","index","gap"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){

        db_utils.check_table(request.table)
        .then(()=>{
            sqlstatement = "SELECT * FROM "+request.table+" WHERE rowid >= ? and rowid < ?"
            db.all(sqlstatement,[request.index,request.index+request.gap],(err,rows)=>{
            if(err) reject(err)
            else{
                sqlstatement = "SELECT COUNT("+db_utils.schem[request.table].primary_key+") AS item_number FROM "+request.table
                db.get(sqlstatement,(err,row)=>{
                    if(err) reject(err)
                    else {
                        let representation_result = {item_number:row.item_number,data:[]}
                        representation_result.data=rows
                        resolve(representation_result)                     
                    }
                })
            }
        })
            
        })
        .catch((err)=>{
            if(err) reject(err)
            else reject({err:"no such table \""+request.table+"\""})
        })

        
    })
}