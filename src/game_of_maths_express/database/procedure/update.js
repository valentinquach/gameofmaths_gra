module.exports.needed_fields = ["table","pk","attributes"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        console.log("update",request)
        db_utils = require("../db_utils.js")
        db_utils.check_table(request.table).then(()=>{
            db = require('../db.js');
            db.get("SELECT * FROM "+request.table+" WHERE "+db_utils.schem[request.table].primary_key+" = ? ",[request.pk],(err,row)=>{
                if(err){
                    reject(err)
                }else if(row){
                    if(Array.isArray(request.attributes)){
                        attributes_names = []
                        attributes_values = []
                        request.attributes.forEach(attribute => {
                            if(attribute.name != undefined){
                                attributes_names.push(attribute.name)
                            }else{
                                reject({err:"each attribute in attribute's field must have a \"name\" attribute"})
                            }
                            if(attribute.value != undefined){
                                attributes_values.push(attribute.value)
                            }else if(attribute.value === null){
                                attributes_values.push(attribute.value)                                    
                            }else{
                                reject({err:"each attribute in attribute's field must have a \"value\" attribute"})
                            }
                        });
                        db_utils.check_attributes(request.table,attributes_names)
                        .then(()=>{
                            sqlStatement = "UPDATE "+request.table+" SET "
                            attributes_names.forEach((name)=>{
                                sqlStatement = sqlStatement+name+" = ? "
                                if(!(name === attributes_names[attributes_names.length-1])){
                                    sqlStatement = sqlStatement+", "
                                }
                            })
                            sqlStatement = sqlStatement+" WHERE "+db_utils.schem[request.table].primary_key+" = ? "
                            attributes_values.push(request.pk)
                            db.run(sqlStatement,attributes_values,(err)=>{
                                if(err) reject(err)
                                else{
                                    resolve({result:"success"}) 
                                } 
                            })
                        })
                        .catch((err)=>{
                            if(err.err){
                                reject(err)
                            }else if(err.attr){
                                reject({err:"no such attribute \""+err.attr+"\""})
                            }
                        })
                    }else{
                        reject({err:"attributes's field of the request must be an array"})
                    }
                }else{
                    reject({err:"no "+request.table+" found with the primary key \""+request.pk+"\""})
                }
            })
        }).catch((err)=>{
            if(err) reject(err)
            else reject({err:"no such table \""+request.table+"\""})
        })
    })
}