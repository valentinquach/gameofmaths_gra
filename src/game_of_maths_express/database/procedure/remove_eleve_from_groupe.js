module.exports.needed_fields = ["groupe","eleve"]
module.exports.process = function(request){
    return new Promise(async function(resolve,reject){
        db = require('../db.js');
        db.run("DELETE FROM AssociationGroupeEleve WHERE groupe_association = ? AND eleve_association = ?",[request.groupe, request.eleve],async (err)=>{
            if(err) reject(err)
            else resolve("success")
        })
    })
}
