const db = require('../db.js')

module.exports.needed_fields = ["pk_classe","pk_theme","visibility"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        if(request.visibility === "MONTRER" || request.visibility === "CACHER"){
            sql_statement = "UPDATE AssociationThemeClasse SET etat_association = ? WHERE theme_association = ? AND classe_association = ?"
            db.run(sql_statement,[request.visibility,request.pk_theme,request.pk_classe],(err)=>{
                if(err) reject(err)
                else resolve({result:"success"})
            })
        }else{
            reject({err:"visibility's field must contains a string like \"MONTRER\" or \"CACHER\""})
        }
    })
}