const db = require('../db')
const db_utils = require('../db_utils')
const generate = require('./get_one')

module.exports.needed_fields = ["index","gap"]
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        db.all("SELECT * FROM Classe WHERE rowid >= ? and rowid < ?",[request.index,request.index+request.gap],(err,rows)=>{
            if(err) reject(err)
            else{
                db.get("SELECT COUNT(id_classe) AS item_number FROM Classe",(err,row)=>{
                    if(err) reject(err)
                    else {
                        let representation_result = {item_number:row.item_number,data:[]}
                        representation_result.data=rows
                        resolve(representation_result)                     
                    }
                })
            }
        })
    })
}