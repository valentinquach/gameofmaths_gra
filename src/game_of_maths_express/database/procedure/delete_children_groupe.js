const db = require("../db.js")
const delete_one = require("./delete_one.js")
const update = require("./update")

module.exports.needed_fields = ["pk"]
module.exports.process = function(request){
    return new Promise((resolve,reject)=>{
        let sql_statement = "SELECT * FROM Chateau WHERE groupe_chateau = ?"
        db.get(sql_statement,[request.pk],(err,row)=>{
            if(err) reject(err)
            else{
                if(row != undefined){
                    update.process({table:"Chateau",pk:row.id_chateau,attributes:[{name:"groupe_chateau",value:null}]})
                    .then(()=>{
                        delete_associations_and_groupe(request.pk)
                        .then(()=>{
                            resolve({result:"success"})
                        })
                        .catch((err)=>reject(err))
                    })
                    .catch((err)=>reject(err))
                }else{
                    delete_associations_and_groupe(request.pk)
                    .then(()=>{
                        resolve({result:"success"})
                    })
                    .catch((err)=>reject(err))
                }
            }
        })
    })
}

function delete_associations_and_groupe(id){
    return new Promise((resolve,reject)=>{        
        sql_statement = "DELETE FROM AssociationGroupeEleve WHERE groupe_association = ?"
        db.run(sql_statement,[id],(err)=>{
            if(err) reject(err)
            else {
                delete_one.process({table:"Groupe",pk:id})
                .then(()=>resolve())
                .catch((err)=>reject(err))
            }
        })
    })
}