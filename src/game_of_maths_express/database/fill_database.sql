DELETE FROM AssociationGroupeEleve;
DELETE FROM AssociationThemeClasse;
DELETE FROM AssociationEleveAttaque;
DELETE FROM AssociationQuestionQuestionnaire;
DELETE FROM Invitation;
DELETE FROM Image;
DELETE FROM Reponse;
DELETE FROM Question;
DELETE FROM Questionnaire;
DELETE FROM Attaque;
DELETE FROM Chateau;
DELETE FROM Classe;
DELETE FROM Groupe;
DELETE FROM Eleve;

INSERT INTO Eleve VALUES(
    '00000000-0000-0000-0000-0000000000000000',
    'prof',
    'prof_mdp',
    'nom_prof',
    'prenom_prof',
    0,
    'prof'
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000001',
    'Chateau 1',
    NULL
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000002',
    'Chateau 2',
    NULL
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000003',
    'Chateau 3',
    NULL
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000004',
    'Chateau 4',
    NULL
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000005',
    'Chateau 5',
    NULL
);

INSERT INTO Chateau VALUES(
    '00000000-0000-0000-0000-0000000000000006',
    'Chateau 6',
    NULL
);