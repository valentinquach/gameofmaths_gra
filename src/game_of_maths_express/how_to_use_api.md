## How to use this api ? (outdated)

### Call the api :

The API respond only to **POST** request, with a specific json body,
see each procedure to know wich field the nested js object must have but here are somes constants : the object must have a field called _procedure_ wich reference to the procedure to use to handle the request, the value must be the name of a procedure file, see the procedure list above.

```js
{
    procedure: "procedure_name"
    //other needed fields
}
```

If an error occur while processing the procedure, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.

In .vue files you could use the fetch module with this syntax :

```js
//fetch option
fetch_option = {
    method: "POST",
    body: JSON.stringify({
        procedure: "procedure_name"
        //other needed fields
    }),
    headers: {
        "content-type": "application/json"
    }
}

//launch the request
fetch("api_url",fetch_option)
.then(res => res.json())
.then(result => {
    //use the respond here
}
```

----

### Procedure list :

<br>

>**get_all**
>
>This procedure allow you to get all rows from a specified table.
>
>The only needed field is the _table_ field, the value must be an existing table in the database.
>
>request object example :
>```js
>{
>   procedure: "get_all",
>   table: "table_name"
>}
>```
>The valid response will be an array where each object is a row from the table. If there is no row in the specified table, the respond will be an empty array.
>
>If the specified table name is not found, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.

<br>

>**get_one**
>
>This procedure allow you to get one specific row from a specified table.
>
>The needed fields are the _table_ and the _pk_ fields, the value of the _table_ must be an existing table in the database, the value of the _pk_ field must be an uuid of a row in the database.
>
>request object example :
>```js
>{
>   procedure: "get_one",
>   table: "table_name",
>   pk: "an_uuid"
>}
>```
>If the specified table name is not found or if there is no row with the specified primary key, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

<br>

>**delete_all**
>
>This procedure allow you to delete all rows from a specified table.
>
>The only needed field is the _table_ field, the value must be an existing table in the database.
>
>request object example :
>```js
>{
>   procedure: "delete_all",
>   table: "table_name"
>}
>```
>The valid response will be a json object with a key _result_ the value of this key will be the String _"success"_ .
>
>If the specified table name is not found, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

<br>

>**delete_one**
>
>This procedure allow you to delete one specific row from a specified table.
>
>The needed fields are the _table_ and the _pk_ fields, the value of the _table_ must be an existing table in the database, the value of the _pk_ field must be an uuid of a row in the database.
>
>request object example :
>```js
>{
>   procedure: "delete_one",
>   table: "table_name",
>   pk: "an_uuid"
>}
>```
>If the specified table name is not found or if there is no row with the specified primary key, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

<br>

>**create**
>
>This procedure allow you to create a row in a specified table.
>
>The needed fields are the _table_ and a field for each attributes of the row except the primary key, the value of the _table_ must be an existing table in the database and each attribute's field must be correct see the database schema to know wich attribute is needed for each table.
>
>request object example :
>```js
>{
>   procedure: "create",
>   table: "table_name",
>   attribute1: "value"
>   //other needed attributes
>}
>```
>If the specified table name is no found or if there is no row with the specified primary key, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

<br>

>**update**
>
>This procedure allow you to update attrivute of a row in a specified table.
>
>The needed fields are the _table_ , the _pk_ and a field for each attributes of the row that you want to update , the value of the _table_ must be an existing table in the database, the value of the _pk_ field must be an uuid of a row in the database and each attribute's field must be correct see the database schema to know wich attribute is needed for each table.
>
> _note : in contrary of the create procedure all attributes don't need to be specified_
>
>request object example :
>```js
>{
>   procedure: "update",
>   table: "table_name",
>   pk: "an_uuid",
>   attribute1: "value"
>   //other attributes to update
>}
>```
>If the specified table name is no found or if there is no row with the specified primary key, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

<br>

>**get_hierarchical_representation_all**
>
>This procedure allow you to get a hierarchical representation of all the data in the tables _Questionnaire_, _Question_ and _Reponse_, wich it should be easier to use.
>
>There is no needed field.
>
>request object example :
>
>```js
>{
>   procedure: "get_hierarchical_representation_all"
>}
>```
>The valid response will be an array where each object is a row from the table _Questionnaire_ and an attribute _questions_ wich is an array who contains each row from the table _Question_ linked with the _Questionnaire_'s row and an attribute _reponses_ wich contains each row from the table _Reponse_ linked with the _Question_'s row. If there is no _questionnaire_ in the database the respond will be an empty array. See the database schema to know wich attribute is in each table.
>
>respond array example :
>
>```js
> [
>   {
>       //attributes of a Questionnaire's row
>       questions: [
>           {
>               //attributes of a Question's row
>               reponses: [
>                   {
>                       //attributes of a Reponse's row
>                   },
>                   //... other reponse(s) of the parent question
>               ]
>           },
>           //... other question(s) of the parent questionnaire
>       ] 
>   },
>   //... other questionnaire(s) in the database
>]
>```

<br>

>**get_hierarchical_representation_one**
>
>This procedure allow you to get a hierarchical representation of a specific _Questionnaire_ and all his linked rows from _Question_ and _Reponse_, wich it should be easier to use.
>
>The only needed field is the _pk_ field, the value must be an uuid of a row in the table _Questionnaire_.
>
>request object example :
>
>```js
>{
>   procedure: "get_hierarchical_representation_one",
>   pk: "an_uuid"
>}
>```
>The valid response will be an object is a row from the table _Questionnaire_ and an attribute _questions_ wich is an array who contains each row from the table _Question_ linked with the _Questionnaire_'s row and an attribute _reponses_ wich contains each row from the table _Reponse_ linked with the _Question_'s row. If there is no _questionnaire_ in the database the respond will be an empty array. See the database schema to know wich attribute is in each table.
>
>respond object example :
>
>```js
>{
>   //attributes of a Questionnaire's row
>   questions: [
>       {
>           //attributes of a Question's row
>           reponses: [
>               {
>                   //attributes of a Reponse's row
>               },
>               //... other reponse(s) of the parent question
>           ]
>       },
>       //... other question(s) of the parent questionnaire
>   ] 
>}
>```

<br>

>**login**
>
>This procedure allow you to request access on the site by loging in.
>
>The needed fields are the _id_ and the _mdp_ fields, the value of the _id_ must be an existing identifiant in the _Eleve_'s table, the value of the _mdp_ field must be an existing password int the _Eleve's_ table in the same row of the specified identifiant.
>
>request object example :
>```js
>{
>   procedure: "login",
>   id: "an_identifiant",
>   mdp: "a_password"
>}
>```
>If the specified identifiant is not found or if the specified password don't correspond of the password in the row of _Eleve_ with the specified identifiant, the respond will be an json object with a key _err_ the value of this key will be a String message with the cause of the error.
>

----
### Database Schema :

The "js object" representation of the database :

```js
{
    Eleve: {
        primary_key:["id_eleve"],
        attributes:[
            "id_login_eleve",
            "mdp_eleve",
            "nom_eleve",
            "prenom_eleve",
            "points_eleve",
            "niveau_eleve",
            "classe_eleve"
        ]
    },
    Chateau: {
        primary_key:["id_chateau"],
        attributes:["maitre_chateau"]
    },
    Attaque: {
        primary_key:["id_attaque"],
        attributes:["chateau_attaque"]
    },
    Theme: {
        primary_key:["id_theme"],
        attributes:["nom_theme"]
    },
    Questionnaire:{
        primary_key:["id_questionnaire"],
        attributes:[
            "titre_questionnaire",
            "theme_questionnaire",
            "temps_questionnaire"
        ]
    },
    Question:{
        primary_key:["id_question"],
        attributes:[
            "titre_question",
            "type_question",
            "theme_question",
            "points_question",
            "bonne_reponse_question"
        ]
    },
    Reponse:{
        primary_key:["id_reponse"],
        attributes:[
            "texte_reponse",
            "numero_reponse",
            "question_reponse"
        ]
    },
    AssociationQuestionQuestionnaire:{
        primary_key:["id_association_question_questionnaire"],
        attributes:[
            "questionnaire_association",
            "question_association"
        ]
    },
    AssociationEleveAttaque:{
        primary:["id_association_eleve_attaque"],
        attributes:["attaque_association","eleve_association"]
    },
    Image:{
        primary_key:["id_image"],
        attributes:[
            "nom_image",
            "the_picture"
        ]
    }
}
```

----

### Edit the api :

To create a new procedure you have to create a file named : <_procedure_name_>.js in _database/procedure/_
The name of the file is using to identify the procedure.

Your procedure must export a _needed_fields_ wich must be an array of String where each String is the name of a needed parameter for the procedure.

Your procedure must export a _process_ function with a parameter which will be the request object, the output of this function will be the respond sent to the client.

exemple of procedure :

```js
//needed fields in the request
module.exports.needed_fields = ["param1","param2"]

//process to exec with the request as a parameter
module.exports.process = function(request){
    return new Promise(function(resolve,reject){
        //resolve or reject response
        if(statement) resolve({result:"success"})
        else reject({err:"error cause"})
    })
}
```

----

### Create and fill the database :

#### Create the db :

Make sure to be in gameofmaths_gra/src/game_of_maths_express/database, and that you have sqlite3 installed.

do :
```js
 sqlite3 -init create_database.sql
```
in the sqlite shell do :
```bash
 .save game_of_maths.db
```

#### Fill the db :

Make sure to be in gameofmaths_gra/src/game_of_maths_express, and that you have node installed.

do :
```js
 node test.js
```

_edit test.js to add more or less rows_

----

_see a [markdown guide](https://about.gitlab.com/handbook/markdown-guide/) for gitlab_
