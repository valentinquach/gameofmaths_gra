const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const {handle} = require("./database/request_handler")
const app = express();

app.use(cors());
app.use(bodyParser.json({limit: '175kb'}));
app.use(bodyParser.urlencoded({limit: '175kb', extended: true}));
app.use(express.json());



app.post('/request',(req,res)=>{
    //TODO A mettre en commentaire si PictureTest.vue
    console.log("request body",req.body)
    handle(req.body)
    .then((request_result)=>{
        //TODO A mettre en commentaire si PictureTest.vue
        console.log("result",request_result)
        res.json(request_result)
    })
    .catch((err)=>{
        console.log(err)
        res.json({"err":err})
    })
})

app.get('/',(req,res) =>{
    res.json({
        err:'please use POST request'
    });
});

const port = process.env.PORT || 4000;

app.listen(port, () =>{
    console.log("listening on port",port);
});
